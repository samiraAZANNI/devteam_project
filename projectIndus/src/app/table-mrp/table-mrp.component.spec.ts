import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableMRPComponent } from './table-mrp.component';

describe('TableMRPComponent', () => {
  let component: TableMRPComponent;
  let fixture: ComponentFixture<TableMRPComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableMRPComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableMRPComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
